/**
 * Project: MAD A2 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-03-13
 * Description: Display yhe details of the item in the list for the Results activity.
 */

package com.example.oliver.pizzaorder;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsList extends Activity {

    TextView text;
    Bundle extras;

    /**
     * Create the DetailsList activity.
     * @param savedInstanceState Any data supplied in onSaveInstanceState.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_list);

        //get the data from the previous activity and display it to the user.
        extras = getIntent().getExtras();
        text = (TextView)findViewById(R.id.detailsView);

        String detailText = "Pizza Number " + extras.getInt("PizzaNumber")+ ". " + extras.getString("WhichToppings");
        text.setText(detailText);
    }
}
