/**
 * Project: MAD A2 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-03-13
 * Description: Display the results of the decisions inputted by the user as well change whether to
 *              have delivery option.
 */

package com.example.oliver.pizzaorder;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.database.Cursor;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * Handle the results of the pizza delivery options
 */
public class Result extends Activity {

    private static final String TAG = "Result";
    private DAL dal;
    Bundle dataBundle = new Bundle();

    // The "total" we display to the end user
    TextView txtTotal;
    ListView PizzaList;
    // The subtotal
    BigDecimal subTotal;
    BigDecimal delivery = new BigDecimal(0);
    BigDecimal deliveryCost = new BigDecimal(5);

    /**
     * Create the Result activity.
     * @param savedInstanceState Any data supplied in onSaveInstanceState.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        dal = new DAL(this);

        Bundle extras = getIntent().getExtras();
        int pizzaCount = extras.getInt("pizzaCount");
        int toppingTotal = extras.getInt("toppingTotal");
        String fullName = extras.getString("name");

        // Find the total textview
        txtTotal = (TextView)findViewById(R.id.TotalValue);
        PizzaList = (ListView)findViewById(R.id.pizzaList);

        dal.openToRead();

        Cursor cursor = dal.queueAll();

        String[] from = new String[]{"Toppings"};
        int[] to = new int[]{R.id.text};

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, R.layout.row, cursor, from, to, 0);

        PizzaList.setAdapter(cursorAdapter);

        dal.close();

        //sets up event handler for the button leading to a website
        Button btnWebsite = (Button)findViewById(R.id.btnWebsite);
        btnWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriUrl = Uri.parse("http://www.pizzahut.com");
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });

        //sets up event handler for the button to open the customer feedback activity
        Button btnCustomerFeedback = (Button)findViewById(R.id.btnCustomerFeedback);
        btnCustomerFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent feedbackView = new Intent(getApplicationContext(), CustomerFeedback.class);
                startActivity(feedbackView);
            }
        });

        //sets up event handler for the button to open phone call
        Button callBtn = (Button)findViewById(R.id.callBtn);
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchphone = new Intent(Intent.ACTION_DIAL);
                launchphone.setData(Uri.parse("tel:5197482702"));
                startActivity(launchphone);
            }
        });

        //sets up event handler for when an item in the list is clicked to go to a screen with
        //more details about the item
        PizzaList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(view.getContext(), DetailsList.class);
                TextView tempText = (TextView)view.findViewById(R.id.text);
                dataBundle.putInt("PizzaNumber", position + 1);
                dataBundle.putString("WhichToppings", tempText.getText().toString());
                intent.putExtras(dataBundle);
                startActivity(intent);
            }
        });


        // Set the delivery handler
        Switch swDelivery = (Switch)findViewById(R.id.switch1);
        swDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (delivery.intValue() == 0)
                {
                    delivery = delivery.add(deliveryCost);
                }
                else
                {
                    delivery = delivery.subtract(deliveryCost);
                }
                txtTotal.setText(NumberFormat.getCurrencyInstance().format(subTotal.add(delivery)));
                Log.i(TAG, "Total Price=" + txtTotal.getText().toString());
            }
        });

        // Set the full name
        TextView txtFullName = (TextView)findViewById(R.id.ConfirmName);
        txtFullName.setText(fullName);

        // Calculate base price
        BigDecimal basePrice = new BigDecimal(pizzaCount).multiply(new BigDecimal("10.00")); // $10.00 per pizza

        // Toppings
        BigDecimal toppingsPrice = new BigDecimal(toppingTotal).multiply(new BigDecimal("1.50")); // $1.50 per topping

        subTotal = basePrice.add(toppingsPrice);

        // Show the price
        txtTotal.setText(NumberFormat.getCurrencyInstance().format(subTotal));

        Log.i(TAG, "Total Price=" + txtTotal.getText().toString());
    }
}
