/**
 * Project: MAD A2 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-03-13
 * Description: A data access layer for stored pizza order information.
 */

package com.example.oliver.pizzaorder;

import java.util.ArrayList;
import java.util.HashMap;
import android.util.Log;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

/**
 * The Data Access Layer
 */
public class DAL {

    private static final String TAG = null;
    private SQLiteHelper sqLiteHelper;
    private SQLiteDatabase sqLiteDatabase;

    private Context context;

    /**
     * Close the database.
     */
    public void close() {
        if (sqLiteHelper != null) {
            sqLiteHelper.close();
        }
    }

    /**
     * Add a pizza to the database.
     * @param content The pizza toppings.
     * @return The id of the new record.
     */
    public long insertPizza(String content) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("Toppings", content);
        this.openToWrite();
        return sqLiteDatabase.insert("Pizzas", null, contentValues);
    }

    /**
     * Delete all of the pizzas.
     * @return The number of rows deleted.
     */
    public int deleteAll() {
        return sqLiteDatabase.delete("Pizzas", null, null);
    }

    /**
     * Get a Cursor for all pizzas.
     * @return A Cursor for the pizzaID and Toppings of all pizzas.
     */
    public Cursor queueAll() {
        String[] columns = new String[]{"_id", "Toppings"};
        Cursor cursor = sqLiteDatabase.query("Pizzas", columns,
                null, null, null, null, null);

        return cursor;
    }

    /**
     * A class to help with creating a SQLite database
     */
    public class SQLiteHelper extends SQLiteOpenHelper {

        /**
         * Initialize the SQLiteHelper
         * @param context The context to use.
         * @param name Name of the database file (null for in memory database)
         * @param factory A CursorFactory for creating Cursors objects (null for the default)
         * @param version The number of the database (starting at 1, will call onUpgrade
         *                or onDowngrade if a different version is detected).
         */
        public SQLiteHelper(Context context, String name,
                            CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        /**
         * Initializes the database (creates all necessary tables).
         * @param db The SQLiteDatabase to use.
         */
        @Override
        public void onCreate(SQLiteDatabase db) {
            String query;
            query = "CREATE TABLE Pizzas (_id INTEGER PRIMARY KEY AUTOINCREMENT, Toppings TEXT);";
            db.execSQL(query);
            Log.i(TAG, "Created successfully");
        }

        /**
         * Handles upgrading the database to a newer version/
         * @param db The SQLiteDatabase to use.
         * @param oldVersion The current version of the database.
         * @param newVersion The new version of the database.
         */
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String query;
            query = "DROP TABLE IF EXISTS Pizzas";
            db.execSQL(query);
            onCreate(db);
        }

    }

    /**
     * Initialize the Data Access Later
     * @param context The context of the DAL
     */
    public DAL(Context context) {
        sqLiteHelper = new SQLiteHelper(context, "pizza.db", null, 2);
    }

    /**
     * Open a readable version of the database.
     * @throws android.database.SQLException
     */
    public void openToRead() throws android.database.SQLException {
        sqLiteDatabase = sqLiteHelper.getReadableDatabase();
    }

    /**
     * Open a writable version of the database.
     * @throws android.database.SQLException
     */
    public void openToWrite() throws android.database.SQLException {
        sqLiteDatabase = sqLiteHelper.getWritableDatabase();
    }
}