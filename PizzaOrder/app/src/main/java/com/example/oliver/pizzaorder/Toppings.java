/**
 * Project: MAD A2 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-03-13
 * Description: Keeps track of toppings on each pizza and is used to tally the total for the cost
 *              of the pizzas.
 */

package com.example.oliver.pizzaorder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Handle the toppings for pizzas
 */
public class Toppings extends Activity {

    private static final String TAG = "Toppings";
    private DAL dal;

    /**
     * The toppings for each pizza.
     */
    class whichToppings{
        public int bacon = 0;
        public int pepperoni = 0;
        public int pineapple = 0;
    }

    Bundle dataBundle = new Bundle();

    Button nxtButton;
    Spinner pizzaNumber;
    ArrayAdapter<String> adapter;
    List<String> spinnerList;

    //list to track each pizzas toppings
    List<whichToppings> listToppings = new ArrayList<whichToppings>();
    CheckBox baconChk;
    CheckBox pepperChk;
    CheckBox pineappleChk;
    String fullName;
    int pizzaCount;
    Bundle extras;
    int toppingTotal = 0;

    /**
     * Create the Toppings activity.
     * @param savedInstanceState Any data supplied in onSaveInstanceState.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toppings);

        dal = new DAL(this);

        //receive data from previous activity
        extras = getIntent().getExtras();
        pizzaCount = extras.getInt("pizzaCount");
        fullName = extras.getString("name");

        //find widgets
        nxtButton = (Button)findViewById(R.id.NextBtn2);
        pizzaNumber = (Spinner)findViewById(R.id.PizzaSpn);
        baconChk = (CheckBox)findViewById(R.id.BaconChk);
        pepperChk = (CheckBox)findViewById(R.id.PepperChk);
        pineappleChk = (CheckBox)findViewById(R.id.PineappleChk);

        spinnerList = new ArrayList<String>();

        //adding the list to the spinner widget
        for(int i=1; i<(pizzaCount+1);i++)
        {
            spinnerList.add("Pizza #" + String.valueOf(i));
            listToppings.add(new whichToppings());
        }

        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, spinnerList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pizzaNumber.setAdapter(adapter);

        //this will change the checkbox to reflect what was saved for each option when an item is
        //selected in the spinner
        pizzaNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int location, long id) {
                whichToppings t = listToppings.get(location);
                baconChk.setChecked(t.bacon != 0);
                pepperChk.setChecked(t.pepperoni != 0);
                pineappleChk.setChecked(t.pineapple != 0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                return;
            }
        });

        pepperChk.setOnCheckedChangeListener(new ToppingChangeListener(ToppingChangeListener.Topping.PEPPERONI, this));
        baconChk.setOnCheckedChangeListener(new ToppingChangeListener(ToppingChangeListener.Topping.BACON, this));
        pineappleChk.setOnCheckedChangeListener(new ToppingChangeListener(ToppingChangeListener.Topping.PINEAPPLE, this));

        nxtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toppingTotal = 0;
                dal.openToWrite();
                dal.deleteAll();

                //count the total number of toppings
                for(int i=0;i<listToppings.size();i++)
                {
                    StringBuilder query = new StringBuilder(100);
                    if(listToppings.get(i).bacon == 1) {
                        query.append("Bacon ");
                    }
                    if(listToppings.get(i).pepperoni==1) {
                        query.append("Pepperoni ");
                    }
                    if(listToppings.get(i).pineapple==1) {
                        query.append("Pineapple ");
                    }
                    if (query.length() == 0) {
                        query.append("No Toppings");
                    }
                    dal.insertPizza(query.toString());
                    toppingTotal += listToppings.get(i).bacon + listToppings.get(i).pepperoni + listToppings.get(i).pineapple;

                }

                dal.close();

                Log.i(TAG, "Total toppings:" + toppingTotal);

                //send the data to the next activity
                Intent intent = new Intent(getApplicationContext(), Result.class);

                dataBundle.putInt("pizzaCount", pizzaCount);
                dataBundle.putString("name", fullName);
                dataBundle.putInt("toppingTotal", toppingTotal);
                intent.putExtras(dataBundle);
                startActivity(intent);

            }
        });
    }

    static class ToppingChangeListener implements CompoundButton.OnCheckedChangeListener {
        public enum Topping {
            PEPPERONI, BACON, PINEAPPLE
        }
        private Topping topping;
        private Toppings activity;

        ToppingChangeListener(Topping topping, Toppings activity) {
            this.topping = topping;
            this.activity = activity;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            whichToppings t = activity.listToppings.get(activity.pizzaNumber.getSelectedItemPosition());
            switch (this.topping) {
                case PEPPERONI:
                    t.pepperoni = isChecked ? 1 : 0;
                    break;
                case PINEAPPLE:
                    t.pineapple = isChecked ? 1 : 0;
                    break;
                case BACON:
                    t.bacon = isChecked ? 1 : 0;
                    break;
            }
        }
    }
}
